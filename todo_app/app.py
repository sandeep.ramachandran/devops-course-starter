
import os
from flask import Flask,redirect
from flask import render_template
from todo_app.data import session_items
from flask import request
from todo_app.data.trello_items import ManageTrelloBoard
from todo_app.flask_config import Config
from todo_app.data.helper import helper

app = Flask(__name__)
app.config.from_object(Config())

#Retrieve Trello Account name, dev key and board name from environment variable
TRELLO_ACTIVE_ACCOUNT_TOKEN = os.getenv('TRELLO_ACCOUNT_TOKEN')
TRELLO_ACTIVE_DEV_KEY = os.getenv('TRELLO_DEV_KEY')
TRELLO_ACTIVE_BOARD_ID = os.getenv('TRELLO_BOARD_ID')

# Default Route
@app.route('/')
def index():
    helper.log_Info("TRELLO_ACTIVE_ACCOUNT_TOKEN", TRELLO_ACTIVE_ACCOUNT_TOKEN)
    helper.log_Info("TRELLO_ACTIVE_DEV_KEY", TRELLO_ACTIVE_DEV_KEY)
    helper.log_Info("TRELLO_ACTIVE_BOARD_ID", TRELLO_ACTIVE_BOARD_ID)
    AllCardList = ManageTrelloBoard.fetch_ToDoCards_OnTheBoard(TRELLO_ACTIVE_BOARD_ID,TRELLO_ACTIVE_ACCOUNT_TOKEN,TRELLO_ACTIVE_DEV_KEY)
    return render_template('index.html', AllIndex=AllCardList)

@app.route('/NewItem', methods=['POST'])
def addNewItem():
    NewItemTitle = request.form['taskName']
    helper.log_Info("Added Tak to To Do List", NewItemTitle)
    if (ManageTrelloBoard.create_NewCards_OnTheBoard(TRELLO_ACTIVE_BOARD_ID,TRELLO_ACTIVE_ACCOUNT_TOKEN,TRELLO_ACTIVE_DEV_KEY,NewItemTitle) == True):
        return redirect("/", code=302)
    else:
        return render_template('404.html'), 404

@app.route('/MarkComplete', methods=['POST'])
def UpdateStatus():
    CardID = request.form['MarkMeDone']
    helper.log_Info("Card ID for which Status need to be updated", CardID)
    if (ManageTrelloBoard.update_itemStatus_OnTheBoard(TRELLO_ACTIVE_BOARD_ID,TRELLO_ACTIVE_ACCOUNT_TOKEN,TRELLO_ACTIVE_DEV_KEY,CardID,"COMPLETE")==True):
        return redirect("/", code=302)
    else:
        return render_template('404.html'), 404