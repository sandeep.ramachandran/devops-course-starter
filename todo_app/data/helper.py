#Helper Class to be used across files and classes
class helper:
    # Flag to be used to assess if any unexpected error has happened
    ERROR_FLAG=False
    def __init__(self) -> None:
        helper.ERROR_FLAG=False

    # Basic logging call. This is used on purpose built calls.
    @classmethod
    def logStatus(self,logTitle,logMessage,additionalInformation):
        print(logTitle + logMessage + additionalInformation)

    # Call used if any standard info need to be logged
    @classmethod
    def log_Info(self,logTitle,logMessage):
        helper.logStatus(logTitle+ "<==INFO ==>",logMessage,"<==")

    # Call to be used if a pass scenario need to be logged. e.g where assertions are needed
    @classmethod
    def log_Pass(self,logTitle,logMessage):
        helper.logStatus(logTitle+ "<==PASS ==>",logMessage,"<==")
    
    # Call to be used if a fail scenario need to be logged. e.g where assertions are needed
    @classmethod
    def log_Fail(self,logTitle,logMessage):
        helper.ERROR_FLAG=True
        helper.logStatus(logTitle + "*******************FAIL******************* ",logMessage," *******************")
    
    # Call to be used if any unexpected scenario occurs
    @classmethod
    def log_Fatal(self,logTitle,logMessage):
        helper.ERROR_FLAG=True
        helper.logStatus(logTitle+"###################FATAL################### ",logMessage," ###################")
    
    # method use to set error flag so subsequent action can be controlled
    @classmethod
    def setErrorFlag(self):
        helper.ERROR_FLAG=True
    
    # method use to reset error flag so subsequent action can be controlled
    @classmethod
    def resetErrorFlag(self):
        helper.ERROR_FLAG=False
    
    # method use to handle error if error flag has been raised
    @classmethod
    def logErrorDetailsIfPresent(self,logMessage):
        if helper.ERROR_FLAG==True:
            helper.logStatus("*******************FAIL******************* ",logMessage," *******************")
