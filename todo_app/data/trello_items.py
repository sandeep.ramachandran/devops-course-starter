import requests
import json
#from helper import helper
from todo_app.data.helper import helper

###############Constant Variable Declarations###############

class ManageTrelloBoard:
    #########List Name Declaration#########
    LIST_TODO_LISTNAME = "To Do"
    LIST_DOING_LISTNAME = "Doing"
    LIST_DONE_LISTNAME = "Done"
    #########List Name Declaration#########

    def __init__(self, id,name,status = "TO DO"):
        self.id=id
        self.name= name
        self.status=status

    @classmethod
    # function to fetch all toDo cards under a board
    def fetch_ToDoCards_OnTheBoard(self,board_ID,token_ID,account_ID):
        # Indicates start of the method for debugging purposes
        helper.log_Info("Start Method Name",self.__name__)
        try:
            #Retrieve card object under the list and return the card dictionary with card name and card ID
            cardsObjectUnderList = ManageTrelloBoard.getCardObject(board_ID,token_ID,account_ID,ManageTrelloBoard.LIST_TODO_LISTNAME)
            cardArray=[]
            cardDict={}
            for cards in cardsObjectUnderList:
                name = cards["name"]
                id = cards["id"]
                cardDict[id] = name
                helper.log_Info("List Name", name)
                helper.log_Info("List Id", id)
                cardArray.append(cardDict)
            return cardDict
        except:
            helper.setErrorFlag()
            helper.log_Fatal("Unexpected Error","Unexpected error while fetching list or card object")
            return False
        finally:
            helper.logErrorDetailsIfPresent("Error @" + self.__name__)
            helper.log_Info("Method Name",self.__name__)

    @classmethod
    # function to create new Ids under a board
    def create_NewCards_OnTheBoard(self,board_ID,token_ID,account_ID,cardName):
        # Indicates start of the method for debugging purposes
        helper.log_Info("Start Method Name",self.__name__)
        try:
            #Get the list object where the card need to be put
            ListObject = ManageTrelloBoard.getListObject(board_ID,token_ID,account_ID)
            ListID=ManageTrelloBoard.getListIDByName(ListObject,ManageTrelloBoard.LIST_TODO_LISTNAME)
            #Construct the request
            toDo_Card_RequestAPI = "https://api.trello.com/1/cards"
            headers = {
                "Accept": "application/json"
            }
            query = {
                'key': account_ID,
                'token': token_ID,
                'name': cardName,
                'idList':ListID
            }
            response = requests.request(
                "POST",
                toDo_Card_RequestAPI,
                headers =headers,
                params=query
            )
            helper.log_Info("Create New Card Response", response.text)     
            return True
        except:
            helper.setErrorFlag()
            helper.log_Fatal("Unexpected Error","Unexpected error while creating new card")
            return False
        finally:
            helper.logErrorDetailsIfPresent("Error @" + self.__name__)
            helper.log_Info("Method Name",self.__name__)

    @classmethod
    # function to update an item status under the list which belongs to a board
    def update_itemStatus_OnTheBoard(self,boardID,token_ID,account_ID,cardID,newStatus):
        # Indicates start of the method for debugging purposes
        helper.log_Info("Start Method Name",self.__name__)
        try:
            # Retrieve the List Object
            ListObj = ManageTrelloBoard.getListObject(boardID,token_ID,account_ID)
            # Retrieve the List ID of the "Done" List
            CompleteListID = ManageTrelloBoard.getListIDByName(ListObj,ManageTrelloBoard.LIST_DONE_LISTNAME)
            toDo_Card_RequestAPI = "https://api.trello.com/1/cards/"+cardID
            headers = {
                "Accept": "application/json"
            }
            query = {
                'key': account_ID,
                'token': token_ID,
                'idList':CompleteListID
            }
            response = requests.request(
                "PUT",
                toDo_Card_RequestAPI,
                headers =headers,
                params=query
            )
            helper.log_Info("Card Status Updated Json", response.text)     
            return True
        except:
            helper.setErrorFlag()
            helper.log_Fatal("Unexpected Error","Unexpected error while updating status of a card")
            return False
        finally:
            helper.logErrorDetailsIfPresent("Error @" + self.__name__)
            helper.log_Info("Method Name",self.__name__)

    # Function to retrieve card Object based on the list name provided
    def getCardObject(board_ID,token_ID,account_ID,ListName):
        try:
            ListObject = ManageTrelloBoard.getListObject(board_ID,token_ID,account_ID)
            for cardStatus in ListObject:
                if cardStatus["name"] == ListName:
                    break
            
            cardsUnderList = cardStatus["cards"]
            return cardsUnderList
        except:
            return False

    # Function to retrieve list Object based on the board Id
    def getListObject(board_ID,token_ID,account_ID):
        try:
            #Construct the request
            toDo_Card_RequestAPI = "https://api.trello.com/1/boards/"+board_ID+"/lists"
            query = {
                'key': account_ID,
                'token': token_ID,
                'cards':'open'
            }
            response = requests.request(
                "GET",
                toDo_Card_RequestAPI,
                params=query
            )
            helper.log_Info("List Object Json", response.text)
            ListObject = json.loads(response.text)
            return ListObject
        except:
            return False

    # Function to retrieve list Id based on the name provided
    def getListIDByName(listObj,listName):
        for listID in listObj:
                if listID["name"] == listName:
                    break
        return listID["id"]